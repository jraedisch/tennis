package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/lightningnetwork/lnd/lnrpc"
)

type Autoresponder struct {
	closingChan chan<- error
	client      *Client
	ctx         context.Context
	lastSend    time.Time
}

func NewAutoresponder(closingChan chan<- error, ctx context.Context, cl *Client) *Autoresponder {
	return &Autoresponder{
		closingChan: closingChan,
		client:      cl,
		ctx:         ctx,
	}
}

func (as *Autoresponder) ClosingChan() <-chan error {
	return make(chan error)
}

func (as *Autoresponder) StartMatch() {
	as.client.PayInvoice(as.ctx, RequestInvoice(Config.OtherPlayers[0].APIendpoint).PayReq)
}

func (as *Autoresponder) Watch() {
	sub, err := as.client.SubscribeInvoices(as.ctx)
	panicOnErr(err)
	for true {
		inv, err := sub.Recv()
		panicOnErr(err)
		go as.handleInvoice(inv)
	}
}

func (as *Autoresponder) handleInvoice(inv *lnrpc.Invoice) {
	// t := time.Now()
	adr := inv.GetFallbackAddr()
	player := Config.PlayerLookup[adr]
	if player != nil {
		if !inv.GetSettled() {
			// log.Printf("not paid yet %d\n%s\n", inv.GetAmtPaid(), player.APIendpoint)
			return
		}
		amtPaid := int64(*as.client.DecodePayReq(inv.PaymentRequest).MilliSat)
		if amtPaid != int64(Config.InitialAmountSatoshis*1000) {
			// log.Printf("invalid incoming amount %d\n%s\n", amtPaid, player.APIendpoint)
			return
		}
		// log.Printf("got %d msats from %s\n", inv.GetAmtPaid(), player.APIendpoint)
		pr := RequestInvoice(player.APIendpoint).PayReq
		amtRequested := int(*as.client.DecodePayReq(pr).MilliSat)
		if Config.InitialAmountSatoshis*1000 != amtRequested {
			// log.Printf("invalid requested amount %d\n%s\n", amtRequested, player.APIendpoint)
			return
		}
		err := as.client.PayInvoice(as.ctx, pr)
		if err != nil {
			as.closingChan <- err
		}
		// log.Printf("handleInvoice %d\n", time.Now().Sub(t).Nanoseconds())
		// log.Printf("paid invoice to player %s\n", adr)
		log.Println(time.Now().Sub(as.lastSend).Nanoseconds() / 1000000)
		as.lastSend = time.Now()
	} else {
		// log.Printf("could not find player %s\n", adr)
	}
}

type Invoice struct {
	PayReq string `json:"pay_req"`
}

func RequestInvoice(url string) *Invoice {
	body := strings.NewReader(fmt.Sprintf(`{"amount":%d, "f":"%s"}`, Config.InitialAmountSatoshis, Config.PublicKey))
	req, err := http.NewRequest(http.MethodPost, url, body)
	if err != nil {
		panic(err)
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}
	dec := json.NewDecoder(resp.Body)
	inv := Invoice{}
	err = dec.Decode(&inv)
	if err != nil {
		panic(err)
	}
	return &inv
}
