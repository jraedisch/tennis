package main

import (
	"context"
	"io/ioutil"

	"github.com/btcsuite/btcd/chaincfg"
	"github.com/lightningnetwork/lnd/lncfg"
	"github.com/lightningnetwork/lnd/lnrpc"
	"github.com/lightningnetwork/lnd/macaroons"
	"github.com/lightningnetwork/lnd/zpay32"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	macaroon "gopkg.in/macaroon.v2"
)

type Client struct {
	rpcClient     lnrpc.LightningClient
	paymentClient lnrpc.Lightning_SendPaymentClient
}

func NewClient(ctx context.Context) *Client {
	creds, err := credentials.NewClientTLSFromFile(Config.TLSCertPath, "")
	if err != nil {
		panic(err)
	}

	// Create a dial options array.
	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(creds),
	}

	macBytes, err := ioutil.ReadFile(Config.AdminMacaroonPath)
	if err != nil {
		panic(err)
	}
	mac := &macaroon.Macaroon{}
	if err = mac.UnmarshalBinary(macBytes); err != nil {
		panic(err)
	}

	macConstraints := []macaroons.Constraint{
		// macaroons.TimeoutConstraint(1),
		// macaroons.IPLockConstraint("127.0.0.1"),
	}

	// Apply constraints to the macaroon.
	constrainedMac, err := macaroons.AddConstraints(mac, macConstraints...)
	if err != nil {
		panic(err)
	}

	// Now we append the macaroon credentials to the dial options.
	cred := macaroons.NewMacaroonCredential(constrainedMac)
	opts = append(opts, grpc.WithPerRPCCredentials(cred))

	// We need to use a custom dialer so we can also connect to unix sockets
	// and not just TCP addresses.
	opts = append(
		opts, grpc.WithDialer(
			lncfg.ClientAddressDialer(Config.RPCServerPort),
		),
	)
	conn, err := grpc.Dial(Config.RPCServerHost, opts...)
	if err != nil {
		panic(err)
	}

	rpcCl := lnrpc.NewLightningClient(conn)
	pCl, err := rpcCl.SendPayment(ctx)
	panicOnErr(err)

	return &Client{rpcClient: rpcCl, paymentClient: pCl}
}

func (cl *Client) SubscribeInvoices(ctx context.Context) (lnrpc.Lightning_SubscribeInvoicesClient, error) {
	req := lnrpc.InvoiceSubscription{}
	req.SettleIndex = cl.GetSettleIndex(ctx)
	// log.Printf("subscribing after settle index %d\n", req.SettleIndex)
	return cl.rpcClient.SubscribeInvoices(ctx, &req)
}

func (cl *Client) GetSettleIndex(ctx context.Context) uint64 {
	req := lnrpc.ListInvoiceRequest{}
	resp, err := cl.rpcClient.ListInvoices(ctx, &req)
	panicOnErr(err)
	var lastSettled uint64 = 0
	for _, inv := range resp.GetInvoices() {
		if inv.GetSettleIndex() > lastSettled {
			lastSettled = inv.GetSettleIndex()
		}
	}
	return lastSettled
}

func (cl *Client) AddInvoice(ctx context.Context, amount int64, d, f string) (string, error) {
	req := lnrpc.Invoice{}
	req.Value = amount
	req.Memo = d
	req.FallbackAddr = f
	resp, err := cl.rpcClient.AddInvoice(ctx, &req)
	panicOnErr(err)
	return resp.GetPaymentRequest(), nil
}

func (cl *Client) PayInvoice(ctx context.Context, payReq string) error {
	// inv, err := zpay32.Decode(payReq, &chaincfg.MainNetParams)
	// panicOnErr(err)
	req := lnrpc.SendRequest{}
	req.PaymentRequest = payReq
	// t := time.Now()
	err := cl.paymentClient.Send(&req)
	// r, err := cl.paymentClient.Recv()
	// r, err := cl.rpcClient.SendPaymentSync(ctx, &req)
	// log.Printf("paytime %d\n", time.Now().Sub(t).Nanoseconds())
	return err
}

func (cl *Client) DecodePayReq(pr string) *zpay32.Invoice {
	inv, err := zpay32.Decode(pr, &chaincfg.MainNetParams)
	panicOnErr(err)
	return inv
}
