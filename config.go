package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
)

func init() {
	configPath := flag.String("config", "config.json", "path to config file")
	flag.Parse()
	// log.Printf("loading config from %s\n", *configPath)

	file, err := ioutil.ReadFile(*configPath)
	panicOnErr(err)
	json.Unmarshal(file, Config)

	// log.Printf("loading %d other players\n", len(Config.OtherPlayers))

	for _, pl := range Config.OtherPlayers {
		Config.PlayerLookup[pl.PublicKey] = pl
	}
}

type ConfigJSON struct {
	EndpointPath          string
	PublicKey             string
	InitialAmountSatoshis int
	ListenIP              string
	ListenPort            string
	ListenProtocol        string
	AdminMacaroonPath     string
	TLSCertPath           string
	OtherPlayers          []*PlayerJSON
	RPCServerPort         string
	RPCServerHost         string
	PlayerLookup          map[string]*PlayerJSON `json:"-"`
	StartMatchPath        string
}

func (cj *ConfigJSON) HostAddress() string {
	return cj.ListenIP + cj.ListenPort
}

type PlayerJSON struct {
	PublicKey   string
	APIendpoint string
}

var Config = &ConfigJSON{
	EndpointPath:          "/payment_requests",
	InitialAmountSatoshis: 1,
	ListenIP:              "0.0.0.0",
	ListenPort:            ":8094",
	ListenProtocol:        "http://",
	AdminMacaroonPath:     "admin.macaroon",
	TLSCertPath:           "tls.cert",
	RPCServerHost:         "localhost",
	RPCServerPort:         "10009",
	PlayerLookup:          make(map[string]*PlayerJSON),
	StartMatchPath:        "/start_match",
}
