package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

func main() {
	// defer profile.Start().Stop()
	closingChan := make(chan error)
	cl := NewClient(context.Background())
	ar := NewAutoresponder(closingChan, context.Background(), cl)
	go ar.Watch()
	go func(ar *Autoresponder) {
		// log.Println("listening on", Config.HostAddress())
		closingChan <- server(Config.HostAddress(), ar, cl).ListenAndServe()
	}(ar)
	panicOnErr(<-ar.ClosingChan())
}

func panicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}

type payReqReq struct {
	Amount int64  `json:"amount"`
	D      string `json:"d"`
	F      string `json:"f"`
}

func server(addr string, ar *Autoresponder, cl *Client) *http.Server {
	m := http.NewServeMux()
	m.HandleFunc(Config.StartMatchPath, func(rw http.ResponseWriter, req *http.Request) {
		// log.Printf("handling call %s %s\n", req.Method, req.URL)
		if req.Method != "GET" {
			http.NotFound(rw, req)
		}
		ar.StartMatch()
	})
	m.HandleFunc(Config.EndpointPath, func(rw http.ResponseWriter, req *http.Request) {
		// log.Printf("handling call %s %s\n", req.Method, req.URL)
		// t := time.Now()
		if req.Method != "POST" {
			http.NotFound(rw, req)
			return
		}
		dec := json.NewDecoder(req.Body)
		rqRq := payReqReq{}
		err := dec.Decode(&rqRq)
		if err != nil {
			// log.Println(err)
			http.Error(rw, "malformed payment request request", 400)
			return
		}
		rq, err := cl.AddInvoice(context.Background(), rqRq.Amount, rqRq.D, rqRq.F)
		if err != nil {
			// log.Println(err)
			http.Error(rw, "cannot create invoice", 500)
			return
		}
		rw.Write([]byte(fmt.Sprintf(`{"pay_req":"%s"}`, rq)))
		// log.Printf("handleRequest %d\n", time.Now().Sub(t).Nanoseconds())
	})
	return &http.Server{Addr: addr, Handler: m}
}
